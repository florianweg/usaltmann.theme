# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from usaltmann.theme.testing import USALTMANN_THEME_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that usaltmann.theme is properly installed."""

    layer = USALTMANN_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if usaltmann.theme is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'usaltmann.theme'))

    def test_browserlayer(self):
        """Test that IUsaltmannThemeLayer is registered."""
        from usaltmann.theme.interfaces import (
            IUsaltmannThemeLayer)
        from plone.browserlayer import utils
        self.assertIn(
            IUsaltmannThemeLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = USALTMANN_THEME_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        roles_before = api.user.get_roles(TEST_USER_ID)
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer.uninstallProducts(['usaltmann.theme'])
        setRoles(self.portal, TEST_USER_ID, roles_before)

    def test_product_uninstalled(self):
        """Test if usaltmann.theme is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'usaltmann.theme'))

    def test_browserlayer_removed(self):
        """Test that IUsaltmannThemeLayer is removed."""
        from usaltmann.theme.interfaces import \
            IUsaltmannThemeLayer
        from plone.browserlayer import utils
        self.assertNotIn(
            IUsaltmannThemeLayer,
            utils.registered_layers())
