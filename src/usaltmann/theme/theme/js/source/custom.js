


$(document).ready(function(){
	
	var query_string = window.location.href;
	query_string = query_string.slice(query_string.search('\\?')+1, query_string.length);
	var artikelnummer = parse_query_string(query_string).artikelnummer;

	if(Boolean(artikelnummer)){
		$('input#topic').val("Anfrage zu Artikel: "+artikelnummer);
		window.scrollTo(0, document.querySelector(".formid-kontakt").offsetTop);
	}


	if($('.slick-big-slider-tile').length > 0){
		$('.slick-big-slider-tile').slick({
			autoplay: true,
			arrows: false,
			infinite: true,
  			slidesToShow: 2,
  			slidesToScroll: 2

		});
	}

})

function parse_query_string(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
      // If second entry with this name
    } else if (typeof query_string[key] === "string") {
      var arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
      // If third or later entry with this name
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}
